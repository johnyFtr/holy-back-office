export interface DayModel {
    id?: string;
    title?: string;
    priority?: string;
    itineraryId?:string;
  }
  