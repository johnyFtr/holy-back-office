export interface ItineraryModel {
    id?: string;
    categoryIteneraryId?: string;
    title?: string;
    subTitle?: string;
    designation?: string;
  }
  