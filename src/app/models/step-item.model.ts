export interface StepItemModel {
    id?: string;
    title?: string;
    priority?: string;
    itineraryId?:string;
    content?: string
  }
  