
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AdminLayoutRoutes } from './admin-layout.routing';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ItineraryComponent } from 'app/pages/itinerary/itinerary.component';
import { CreateItineraryComponent } from 'app/pages/create-itinerary/create-itinerary.component';
import { DemoMaterialModule } from 'app/material-module';
import { MatNativeDateModule } from '@angular/material/core';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from '@angular/material/form-field';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { DialogGalleryComponent } from 'app/component/dialog-gallery/dialog-gallery.component';
import { DialogStepComponent } from 'app/component/dialog-step/dialog-step.component';
import { ImageLazyLoadModule } from 'app/lazy-image/image-lazy-load.module';
import { CreateDayComponent } from 'app/pages/create-day/create-day.component';
import { CreateStepComponent } from 'app/pages/create-step/create-step.component';
import { ImageShowComponent } from 'app/pages/image/showImage/show.component';
import { ImageComponent } from 'app/pages/image/image.component';
import { DialogComponent } from 'app/pages/image/dialog/dialog.component';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    RouterModule.forChild(AdminLayoutRoutes),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    DemoMaterialModule,
    MatNativeDateModule,
    ImageLazyLoadModule,
    NgxDropzoneModule

  ],
  declarations: [
    ImageShowComponent,
    ImageComponent,
    DialogComponent,
    DashboardComponent,
    ItineraryComponent,
    CreateItineraryComponent,
    DialogGalleryComponent,
    DialogStepComponent,
    CreateDayComponent,
    CreateStepComponent
  ],
  providers: [
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ],
})

export class AdminLayoutModule { }
