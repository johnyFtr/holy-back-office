import { ImageComponent } from './../../pages/image/image.component';
import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { ItineraryComponent } from 'app/pages/itinerary/itinerary.component';
import { CreateItineraryComponent } from 'app/pages/create-itinerary/create-itinerary.component';
import { CreateDayComponent } from 'app/pages/create-day/create-day.component';
import { CreateStepComponent } from 'app/pages/create-step/create-step.component';

export const AdminLayoutRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: 'itinerary', component: ItineraryComponent },
  { path: 'new-itinerary', component: CreateItineraryComponent },
  { path: 'gallery', component: ImageComponent },
  { path: 'new-itinerary/create-day', component: CreateDayComponent },
  { path: 'new-itinerary/create-step', component: CreateStepComponent },
];
