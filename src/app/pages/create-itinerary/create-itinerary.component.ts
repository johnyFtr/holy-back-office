import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { DialogGalleryComponent } from 'app/component/dialog-gallery/dialog-gallery.component';
import { DialogStepComponent } from 'app/component/dialog-step/dialog-step.component';
import { ItineraryService } from 'app/services/itinerary.service';

export interface DialogData {
  animal: string;
  name: string;
}


@Component({
  selector: 'app-create-itinerary',
  templateUrl: './create-itinerary.component.html',
  styleUrls: ['./create-itinerary.component.css']
})
export class CreateItineraryComponent implements OnInit {
  stepForm: FormGroup;
  listeCategoryStep: any;
  stepList: [];
  imagesChoose = [];

  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router,
    private itineraryService: ItineraryService
  ) { }

  ngOnInit(): void {
    this.stepForm = this.formBuilder.group({
      categoryStep: [null, Validators.required],
      designationIt: [null, Validators.required],
      titleIt: [null, Validators.required],
      subTitleIt: [null, Validators.required],
      pictureIt: '',
      // dayStep: this.formBuilder.array([])
    });

    this.fetchAllCategoryItinerary();
  }

  // dynamic form
  initGroupStep() {
    let stepDate = this.stepForm.get("dayStep") as FormArray;
    // Champs dynamique
    stepDate.push(
      this.formBuilder.group({
        titleStep: [null, Validators.required],
        priorityStep: [null, Validators.required]
      })
    );
  }

  // delete day
  onDeleteRow(rowIndex) {
    let rows = this.stepForm.get("dayStep") as FormArray;
    rows.removeAt(rowIndex);
  }

  // open dialog step item
  openDialogStep(): void {
    const dialogRef1 = this.dialog.open(DialogStepComponent, {
      width: '700px',
      height: '600px',
    });
    dialogRef1.afterClosed().subscribe(result => {
      this.stepList = result['dayStep'];
    });
  }

  // open dialog step gallery image
  openDialogGallery(): void {
    const dialogRef2 = this.dialog.open(DialogGalleryComponent, {
      width: '900px',
      height: '600px',
    });
    dialogRef2.afterClosed().subscribe(result => {
      this.imagesChoose = result;
    });
  }

  // save new itinerary
  async onSubmitItinerary() {
    if (this.stepForm.valid) {
      const itinerary = {
        "designation": this.stepForm.get('designationIt').value,
        "title": this.stepForm.get('titleIt').value,
        "subTitle": this.stepForm.get('subTitleIt').value,
        "categorieItineraryId": this.stepForm.get('categoryStep').value,
        "imageFavori": this.imagesChoose[0]
      }

      await this.createItinerary(itinerary);
      const itineraryId = localStorage.getItem("itineraryId");
      for (let i = 1; i < this.imagesChoose.length; i++) {
        await this.addImageItinerary(itineraryId, this.imagesChoose[i]);
      }
      this.router.navigateByUrl(`/new-itinerary/create-day`);
    } else {
      console.log("invalid");
    }
  }

  fetchAllCategoryItinerary() {
    this.itineraryService.getCategorieItinerary().then(category => {
      this.listeCategoryStep = category;
    }).catch(error => {
      console.log(error.message);
    });
  }

  createItinerary(itinerary) {
    this.itineraryService.setItinerary(itinerary).then(res => {
      localStorage.setItem('itineraryId', res.result.id);
    }).catch(error => {
      console.log(error.message);
    });
  }

  addImageItinerary(itineraryId, idImage) {
    this.itineraryService.setImageItineraryImage(itineraryId, idImage).then(res => {
      console.log(res);
    }).catch(error => {
      console.log(error.message);
    });
  }
}



