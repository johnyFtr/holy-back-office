import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ItineraryService } from 'app/services/itinerary.service';
import { DialogGalleryComponent } from 'app/component/dialog-gallery/dialog-gallery.component';

@Component({
  selector: 'app-create-step',
  templateUrl: './create-step.component.html',
  styleUrls: ['./create-step.component.css']
})
export class CreateStepComponent implements OnInit {
  stepForm: FormGroup;
  etatStep: boolean = false;
  imagesChoosen = [];

  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router,
    private itineraryService: ItineraryService 
  ) { }

  ngOnInit(): void {
    this.stepForm = this.formBuilder.group({
      titleStep: [null, Validators.required],
      priorityStep: [null, Validators.required],
      descriptionStep: [null, Validators.required],
      pictureStep: "",
    })

    this.stepForm.reset();
  }

  async onSubmitStep() {
    if (this.stepForm.valid) {
      const dayStep = {
        "title": this.stepForm.get("titleStep").value,
        "priority": this.stepForm.get("priorityStep").value,
        "content": this.stepForm.get("descriptionStep").value,
        "stepId":  localStorage.getItem('dayId')
      };
      this.etatStep = true;

      await this.createStep(dayStep);
      const stepId = localStorage.getItem("StepOfStepId");
      for(const imageId of this.imagesChoosen){
         await this.addImageStepOfStep(stepId, imageId);
      }
      // this.router.navigateByUrl(`/itinerary/create-step`);
    }
  }

  onDayClick(){
    this.etatStep = true;
    this.stepForm.reset();
  }
  
  onStepClick(){
    this.etatStep = false;
    this.stepForm.reset();
  }

  createStep(step){
    this.itineraryService.setStepOfStep(step).then(res => {
      localStorage.setItem('StepOfStepId', res.id);
    }).catch(error => {
      console.log(error.message);
    });
  }

    // open dialog step gallery image
    openDialogGallery(): void {
      const dialogRef2 = this.dialog.open(DialogGalleryComponent, {
        width: '900px',
        height: '600px',
      });
      dialogRef2.afterClosed().subscribe(result => {
         this.imagesChoosen = result;
      });
    }

    addImageStepOfStep(stepId, idImage){
      this.itineraryService.imageStepOfStep(stepId, idImage).then(res => {
        console.log(res);
      }).catch(error => {
        console.log(error.message);
      });
    }

}
