import { DialogComponent } from './dialog/dialog.component';
import { GalleryService } from './../../services/gallery.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

  images: any;

  categories: any;

  constructor(
    private galleryService: GalleryService,
    public dialog: MatDialog
  ) { }

  openDialog() {
    const dialog = this.dialog.open(DialogComponent, {
      width: '60%',
      minWidth: '600px',
    });
    dialog.afterClosed().subscribe(results => {
      this.saveData(results);
    })
  }

  // {categorieImageId: "1", name: "fsdf", description: "dsasd", littleDescription: "dasdasd", images: Array(1)}
  // categorieImageId: "1"
  // description: "dsasd"
  // images: ["https://firebasestorage.googleapis.com/v0/b/image-…=media&token=1ecaf6ac-8362-4aad-8a51-0daeca599b44"]
  // littleDescription: "dasdasd"
  // name: "fsdf"
  // __proto__: Object

  saveData(data) {
    const information = {
      categorieImageId: data.categorieImageId,
      description: data.description,
      littleDescription: data.littleDescription,
      name: data.description,
      url: data.images[0],
      favori: true,
      video: data.video
    };
    this.galleryService.saveImage(information).then(async (res) => {
      if (res['status'] === 201) {
        const imageId = res['data']['id'];
        for (let i = 1; i < data.images.length; i++) {
          let temp = {
            categorieImageId: data.categorieImageId,
            name: '',
            description: '',
            littleDescription: '',
            url: data.images[i],
            imageId,
            favori: false,
            video: ''
          }
          await this.galleryService.saveImage(temp);
        }
      } else {
        console.log('error registration of this image.');
      }
    })
  }

  onCategorieChange(value) {
    if (value !== '') {
      this.galleryService.getImagesByCategorie(value).then(images => {
        this.images = images;
      }).catch(err => {
        alert(err.message);
      });
    }
  }

  ngOnInit(): void {
    // get all images
    this.galleryService.getImages().then(images => {
      this.images = images;
      console.log(images);
    }).catch(err => {
      console.log(err.message);
    });

    // get all categorie
    this.galleryService.getCategorie().then(categories => {
      this.categories = categories;
    }).catch(err => {
      console.log(err.message);
    });
  }

}
