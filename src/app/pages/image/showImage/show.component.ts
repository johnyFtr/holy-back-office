import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-show-image',
  templateUrl: './show.component.html',
})
export class ImageShowComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<ImageShowComponent>
  ) { }

  ngOnInit() {

  }
}
