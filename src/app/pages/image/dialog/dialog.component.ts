import { Validators } from '@angular/forms';
import { GalleryService } from './../../../services/gallery.service';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: 'dialog.component.html',
})
export class DialogComponent implements OnInit {

  files: File[] = [];

  categories: any;
  images = [];

  ImageForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    private formBuilder: FormBuilder,
    private galleryService: GalleryService
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  async onFormSubmit() {
    if (this.files.length > 0 && this.ImageForm.valid) {
      for (const file of this.files) {
        this.images.push(await this.galleryService.uploadImage(file));
      }
      const renderData = {
        ...this.ImageForm.value,
        images: this.images
      }
      this.dialogRef.close(renderData);
    } else {
      alert('There are information missing');
    }
  }

  ngOnInit(): void {
    this.ImageForm = this.formBuilder.group({
      categorieImageId: [null, Validators.required],
      name: [null, Validators.required],
      description: [null, Validators.required],
      littleDescription: [null, Validators.required],
      video: [null, Validators.required]
    });

    this.galleryService.getCategorie().then(results => {
      this.categories = results;
    }).catch(err => { alert(console.log(err.message)) });
  }

  onSelect(event) {
    this.files.push(...event.addedFiles);
  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }

}


