import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ItineraryService } from 'app/services/itinerary.service';

@Component({
  selector: 'app-create-day',
  templateUrl: './create-day.component.html',
  styleUrls: ['./create-day.component.css']
})
export class CreateDayComponent implements OnInit {
  dayForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router,
    private itineraryService: ItineraryService 
  ) { }

  ngOnInit(): void {
    this.dayForm = this.formBuilder.group({
      titleStep: [null, Validators.required],
      priorityStep: [null, Validators.required],
    })
    this.dayForm.reset();
  }
  async onSubmitDay() {
    if (this.dayForm.valid) {
      const dayStep = {
        "title": this.dayForm.get("titleStep").value,
        "priority": this.dayForm.get("priorityStep").value,
        "itineraryId": localStorage.getItem('itineraryId')
      };
      await this.createDay(dayStep);
      this.router.navigateByUrl(`/new-itinerary/create-step`);
    }
  }

  createDay(step){
    this.itineraryService.setStepItinerary(step).then(res => {
      localStorage.setItem('dayId', res.id);
    }).catch(error => {
      console.log(error.message);
    });
  }

}
