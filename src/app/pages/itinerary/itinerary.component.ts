import { Component, OnInit } from '@angular/core';
import { ItineraryService } from 'app/services/itinerary.service';

@Component({
  selector: 'app-itinerary',
  templateUrl: './itinerary.component.html',
  styleUrls: ['./itinerary.component.css']
})
export class ItineraryComponent implements OnInit {
  itineraryList = [];
  constructor(
    private itineraryService: ItineraryService
  ) { }

  ngOnInit(): void {
    this.fetchAllItinerary();
  }

  
  fetchAllItinerary(){
    this.itineraryService.getItineraries().then(itinerary => {
      this.itineraryList = itinerary;
    }).catch(error => {
      console.log(error.message);
    });
  }

}
