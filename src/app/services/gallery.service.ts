import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import * as firebase from 'firebase';
import Axios from 'axios';

@Injectable({
  providedIn: 'root'
})
export class GalleryService {

  images: any;

  saveImage(data) {
    return new Promise((resolve, rejects) => {
      Axios.post(environment.apiUrl + '/image', {
        categorieImageId : data.categorieImageId,
        name: data.name,
        url: data.url,
        littleDescription: data.littleDescription,
        description: data.description,
        imageId : data.imageId ? data.imageId : null,
        favori: data.favori,
        video: data.video
      }).then(res => {
        resolve(res);
      }).catch(err => {
        rejects(null);
        console.log(err.message);
      });
    })
  }

  uploadImage(file): Promise<any> {
    return new Promise((resolve, rejects) => {
      const uniqueName = Date.now().toString();
      const upload = firebase.storage().ref().child('image/' + uniqueName + file.name).put(file);
      upload.on(firebase.storage.TaskEvent.STATE_CHANGED, () => {
        console.log('loading ...');
      }, (err) => {
        rejects(err);
        console.log('error loading ...')
      }, () => {
        console.log('this file was uploaded.');
        upload.snapshot.ref.getDownloadURL().then(downloadURL => {
          const imageUrl = downloadURL;
          console.log('URL:' + imageUrl);
          resolve(imageUrl);
        });
      });
    });
  }

  getImagesByCategorie(categorieId): Promise<any> {
    return new Promise((resolve, rejects) => {
      fetch(environment.apiUrl + '/image/categorie/' + categorieId).then(res => {
        res.json().then(results => {
          resolve(results);
        }).catch(err => {
          rejects(err);
        });
      }).catch(err => {
        rejects(err);
      });
    });
  }

  getCategorie(): Promise<any> {
    return new Promise((resolve, rejects) => {
      fetch(environment.apiUrl + '/categorieImage').then(res => {
        res.json().then(results => {
          resolve(results);
        }).catch(err => {
          rejects(err);
        }).catch(err => {
          rejects(err);
        });
      });
    });
  }

  getImages(): Promise<any> {
    return new Promise((resolve, rejects) => {
      fetch(environment.apiUrl + '/image').then(res => {
        res.json().then(results => {
          resolve(results);
        }).catch(err => {
          rejects(err);
        })
      }).catch(err => {
        rejects(err);
      });
    });
  }
  constructor() { }
}
