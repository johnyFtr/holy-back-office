import { Injectable } from '@angular/core';
import Axios from 'axios';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ItineraryService {
  constructor() { }

  getItineraries(): Promise<any> {
    return new Promise((resolve, rejects) => {
      Axios.get(environment.apiUrl + '/itinerary').then(res => {
        resolve(res.data);
      }).catch(err => {
        console.log(err.message);
        rejects(null);
      })
    });
  }

  setItinerary(itinerary): Promise<any> {
    return new Promise((resolve, rejects) => {
      Axios.post(environment.apiUrl + '/itinerary', itinerary).then(res => {
        resolve(res.data);
      }).catch(err => {
        console.log(err.message);
        rejects(null);
      })
    });
  }

  getItineraryById(id): Promise<any> {
    return new Promise((resolve, rejects) => {
      Axios.get(environment.apiUrl + '/itinerary/' + id).then(res => {
        resolve(res.data);
      }).catch(err => {
        console.log(err.message);
        rejects(null);
      })
    });
  }

  getCategorieItinerary(): Promise<any> {
    return new Promise((resolve, rejects) => {
      Axios.get(environment.apiUrl + '/categorieItinerary').then(res => {
        resolve(res.data);
      }).catch(err => {
        console.log(err.message);
        rejects(null);
      })
    })
  }

  setImageItineraryImage(itineraryId, idImage): Promise<any> {
    return new Promise((resolve, rejects) => {
      Axios.patch(environment.apiUrl + '/image/' + idImage, { itineraryId }).then(res => {
        resolve(res.data);
      }).catch(err => {
        console.log(err.message);
        rejects(null);
      });
    });
  }

  setStepItinerary(data): Promise<any> {
    return new Promise((resolve, rejects) => {
      Axios.post(environment.apiUrl + '/step', {
        title: data.title,
        priority: data.priority,
        itineraryId: data.itineraryId
      }).then(res => {
        resolve(res.data);
      }).catch(err => {
        rejects(null);
        console.log(err.message);
      });
    });
  }

  setStepOfStep(data): Promise<any> {
    return new Promise((resolve, rejects) => {
      Axios.post(environment.apiUrl + '/step', {
        title: data.title,
        priority: data.priority,
        stepId: data.stepId,
        content: data.content
      }).then(res => {
        resolve(res.data);
      }).catch(err => {
        rejects(null);
        console.log(err.message);
      })
    });
  }

  imageStepOfStep(stepId, imageId): Promise<any> {
    return new Promise((resolve, rejects) => {
      Axios.patch(environment.apiUrl + '/image/' + imageId, { stepId }).then(res => {
        resolve(res.data);
      }).catch(err => {
        rejects(null);
        console.log(err.message);
      });
    });
  }

}
