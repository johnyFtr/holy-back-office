import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor() {
    const firebaseConfig = {
      apiKey: 'AIzaSyD5rd9hs0Wv1v6m__xnX7Os1bMShRprPwY',
      authDomain: 'image-store-2a603.firebaseapp.com',
      databaseURL: 'https://image-store-2a603.firebaseio.com',
      projectId: 'image-store-2a603',
      storageBucket: 'image-store-2a603.appspot.com',
      messagingSenderId: '457357179558',
      appId: '1:457357179558:web:a83495dbccf4f92a155267',
      measurementId: 'G-D6XQLD0SCF'
    };
    firebase.initializeApp(firebaseConfig);
    firebase.analytics();
  }
}
