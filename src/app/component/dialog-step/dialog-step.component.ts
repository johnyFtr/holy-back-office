import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogGalleryComponent } from '../dialog-gallery/dialog-gallery.component';


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-dialog-step',
  templateUrl: './dialog-step.component.html',
  styleUrls: ['./dialog-step.component.css']
})
export class DialogStepComponent implements OnInit {
  stepItemForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<DialogStepComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private formBuilder: FormBuilder,
    public dialog: MatDialog,
    private router: Router,
    private route: ActivatedRoute,) { }

  onNoClick(): void {
    this.dialogRef.close();
  }


  ngOnInit(): void {
    this.stepItemForm = this.formBuilder.group({
      dayStep: this.formBuilder.array([])
    });
  }

  // dynamic form
  initGroup() {
    let stepDate = this.stepItemForm.get("dayStep") as FormArray;
    // Champs dynamique
    stepDate.push(
      this.formBuilder.group({
        titleStep: [null, Validators.required],
        priorityStep: [null, Validators.required],
        descriptionStep: [null, Validators.required],
        pictureStep: [null],
      })
    );
  }

  // delete day
  onDeleteRow(rowIndex) {
    let rows = this.stepItemForm.get("dayStep") as FormArray;
    rows.removeAt(rowIndex);
  }

  // save itinerary
  onSubmitForm() {
    this.dialogRef.close(this.stepItemForm.value);
  }

  // open dialog step gallery image
  openDialogGallery(): void {
    const dialogRef = this.dialog.open(DialogGalleryComponent, {
      width: '900px',
      height: '600px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

}
