import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { GalleryService } from 'app/services/gallery.service';




@Component({
  selector: 'app-dialog-gallery',
  templateUrl: './dialog-gallery.component.html',
  styleUrls: ['./dialog-gallery.component.css']
})
export class DialogGalleryComponent implements OnInit {
  images;
  imageChoosen = [];
  constructor(
    public dialogRef: MatDialogRef<DialogGalleryComponent>,
    public galleryService: GalleryService,
    public dialog: MatDialog) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit(): void {
    this.fetchAllImage();
  }

  fetchAllImage() {
    this.galleryService.getImages().then(images => {
      this.images = images
    }).catch(error => {
      console.log(error.message);
    });
  }

  chooseImage(id) {
    if (this.imageChoosen.indexOf(id) === -1) {
      this.imageChoosen.push(id);
    } else {
      this.imageChoosen = this.imageChoosen.filter(n => n !== id);
    }
    console.log(this.imageChoosen);
  }


  // save itinerary
  valideClick() {
    this.dialogRef.close(this.imageChoosen);
  }

}
